import React, { Component } from 'react';
import ChatBot from 'react-simple-chatbot';
import Review from './Review';

class SimpleForm extends Component {
    render() {
      return (
        <ChatBot
          steps={[
            {
              id: '1',
              message: 'What is your name?',
              trigger: 'name',
            },
            {
              id: 'name',
              user: true,
              trigger: '3',
            },
            {
              id: '3',
              message: 'Hi {previousValue}! In which domain you want to work?',
              trigger: 'domain',
            },
            {
              id: 'domain',
              options: [
                { value: 'Web Developer', label: 'Web Developer', trigger: '5' },
               { value: 'Android Developer', label: 'Android Developer', trigger: '6' },
               { value: 'Full Stack Developer', label: 'Full Stack Developer', trigger: '7' },
              { value: 'UI/UX Designer', label: 'UI/UX Designer', trigger: '8' },

              ],
            },
            {
              id: '5',
              message: 'On which web frameworks do you want to work?',
              trigger: 'web-frameworks',
            },
            {
              id: 'web-frameworks',
              options:[
              { value: 'ReactJS', label: 'ReactJS', trigger: '9' },
              { value: 'Angular', label: 'Angular', trigger: '9' },
              { value: 'VueJS', label: 'VueJS', trigger: '9' },
              { value: 'Django', label: 'Django', trigger: '9' },
              { value: 'Laravel', label: 'Laravel', trigger: '9' },
              { value: 'Spring', label: 'Spring', trigger: '9' },
            ]
            },
            {
              id:'9',
              message:'How many years of experience you have in the related framework?',
              trigger:'experience'
            },
            {
              id:'experience',
              user:true,
              trigger:'12',
              validator: (value) => {
                if (isNaN(value)) {
                  return 'value must be a number';
                } else if (value < 0) {
                  return 'value must be positive';
                } else if (value > 120) {
                  return `${value}? Come on!`;
                }
  
                return true;
              },
            },
            {
              id: '6',
              message: 'On which android frameworks do you want to work?',
              trigger: 'android-frameworks',
            },
            {
              id: 'android-frameworks',
              options:[
              { value: 'React-native', label: 'React-native', trigger: '9' },
              { value: 'Flutter', label: 'Flutter', trigger: '9' },
              { value: 'Ionic', label: 'Ionic', trigger: '9' },
              { value: 'Xamarin', label: 'Xamarin', trigger: '9' },
              { value: 'Native Android', label: 'Native Android', trigger: '9' },
            ]
            },
            {
              id: '7',
              message: 'On which frameworks do you want to work?',
              trigger: 'stack-frameworks',
            },
            {
              id: 'stack-frameworks',
              options:[
              { value: 'NodeJS', label: 'NodeJS', trigger: '9' },
              { value: 'Ruby on Rails', label: 'Ruby on Rails', trigger: '9' },
              { value: 'Django', label: 'Django', trigger: '9' },
              { value: 'SpringBoot', label: 'SpringBoot', trigger: '9' },
            ]
            },
            {
              id: '8',
              message: 'On which UI/UX frameworks do you want to work?',
              trigger: 'ui/ux-frameworks',
            },
            {
              id: 'ui/ux-frameworks',
              options:[
              { value: 'Bootstrap', label: 'Bootstrap', trigger: '9' },
              { value: 'Milligram', label: 'Milligram', trigger: '9' },
              { value: 'Semantic-UI', label: 'Semantic-UI', trigger: '9' },
              { value: 'Materilaize', label: 'Materilaize', trigger: '9' },
              { value: 'Skeleton', label: 'Skeleton', trigger: '9' },
            ]
            },
            {
              id:'12',
              message:'In which location do you want to work',
              trigger:'location'
            },
            {
              id:'location',
              options:[
                { value: 'Hyderabad', label: 'Hyderabad', trigger: '13' },
                { value: 'Indore', label: 'Indore', trigger: '13' },
              ],
              trigger:'13',
             },

            {
              id: '13',
              message: 'Enter your email so we can keep you in update',
              trigger: 'email',
            },
            {
              id: 'email',
              user:true,
              trigger: '14',
            },
            {
              id: '14',
              message: 'Great! Check out your summary',
              trigger: 'review',
            },
            {
              id: 'review',
              component: <Review />,
              asMessage: true,
              trigger: 'update',
            },

            {
              id: 'update',
              message: 'Would you like to update some field?',
              trigger: 'update-question',
            },
            {
              id: 'update-question',
              options: [
                { value: 'yes', label: 'Yes', trigger: 'update-yes' },
                { value: 'no', label: 'No', trigger: 'end-message' },
              ],
            },
            {
              id: 'update-yes',
              message: 'What field would you like to update?',
              trigger: 'update-fields',
            },
            {
              id: 'update-fields',
              options: [
                { value: 'name', label: 'Name', trigger: 'update-name' },
                { value: 'experience', label: 'experience', trigger: 'update-experience' },
                { value: 'location', label: 'location', trigger: 'update-location' },
                { value: 'email', label: 'email', trigger: 'update-email' },
              ],
            },
            {
              id: 'update-name',
              update: 'name',
              trigger: '14',
            },
            {
              id: 'update-experience',
              update: 'experience',
              trigger: '14',
            },
            {
              id: 'update-location',
              update: 'location',
              trigger: '14',
            },
            {
              id: 'update-email',
              update: 'email',
              trigger: '14',
            },
            {
              id: 'end-message',
              message: 'Thanks! Your data was recorded and we will keep you updated with the respective email',
              end: true,
            },
          ]}
        />
      );
    }
  }
  
  export default SimpleForm;
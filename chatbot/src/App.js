import React, { useState } from 'react';
import SimpleForm from './SimpleForm';
import './App.css';

const App = (props) => {
  let [showChat, setShowChat] = useState(false);

  const startChat = () => { setShowChat(true); }
  const hideChat = () => { setShowChat(false); }

    return (
      <>
      <div className = "header">
        <h2>CHATBOT</h2>
      </div>
      <div className = "main">
        <div className ="nav">
          <img src="https://www.comm100.com/wp-content/uploads/images/blog-banner-ai-powered-chatbot-1.png" alt="chatbot" className="chatbot-img"/>
        </div>
        <div className ="content">
          <div style = {{padding:"20px"}}>
            <p>A chatbot is an artificial intelligence (AI) software that can simulate a conversation (or a chat) with a user in natural language through messaging applications, websites, mobile apps or through the telephone</p>
            <p>Chatbot or bot – is a computer program that simulates a natural human conversation. Users communicate with a chatbot via the chat interface or by voice, like how they would talk to a real person. Chatbots interpret and process user's words or phrases and give an instant pre-set answer. </p>
          </div>
        </div>
      </div>
      <div className = "bot">
        <div style ={{display: showChat ? "" : "none"}}>
          <SimpleForm></SimpleForm>
        </div>      
        {/* <div> {showChat ? <SimpleForm></SimpleForm> : null} </div> */}
        <div>
          {!showChat 
            ? <button className="btn" onClick={() => startChat()}>Click to Chat </button> 
            : <button className="btn" onClick={() => hideChat()}>Click to Hide </button>}
        </div>
      </div>      
      </>
    )
}

export default App;
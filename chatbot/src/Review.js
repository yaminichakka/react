import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

const Review = (props)  => {
  const [state, setState] = useState({ name: '', domain: '',experience :'', location:'',email:''});
  
  useEffect(() => {
    const { steps } = props;
    const { name, domain , experience , location,email } = steps;
    setState({ name, domain,experience , location,email});
  }, [props])

    const { name, domain , experience , location, email } = state;
    return (
      <div style={{ width: '100%' }}>
        <h3>Summary</h3>
        <table>
          <tbody>
            <tr>
              <td>Name</td>
              <td>{name.value}</td>
            </tr>
            <tr>
              <td>Domain</td>
              <td>{domain.value}</td>
            </tr>
            <tr>
              <td>Experience</td>
              <td>{experience.value}</td>
            </tr>
            <tr>
              <td>Location</td>
              <td>{location.value}</td>
            </tr>
            <tr>
              <td>Email</td>
              <td>{email.value}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
}

Review.propTypes = {
  steps: PropTypes.object,
};

Review.defaultProps = {
  steps: undefined,
};

export default Review;
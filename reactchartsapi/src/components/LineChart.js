import React,{useState,useEffect} from 'react';
import {Line} from 'react-chartjs-2';
import axios from 'axios';

const LineChart = () => {
    const [chartData,setChartData] = useState({});
    // const [employeeSalary,setEmployeeSalary] = useState([]);
    // const [employeeAge,setEmployeeAge] = useState([]);

const chart = () =>
{
    let empAge = [];
    let empSal = [];
    axios.get("http://dummy.restapiexample.com/api/v1/employees")
    .then(res=>{
        console.log(res);
        for(const dataObj of res.data.data){
            empSal.push(parseInt(dataObj.employee_salary));
            empAge.push(parseInt(dataObj.employee_age));
        }
    console.log(empSal,empAge);
    setChartData({
        labels:empAge,
        datasets:[{
        label:"Number of items",
        data:empSal,
        backgroundColor:["rgba(75,192,192,0.6"],
        borderWidth:4
    }]
    })
    })
    .catch(err => {
        console.log(err)
    })
    console.log(empAge,empSal);
}
useEffect(()=>{
    chart();
},[]);
return (
    <Line
    data={chartData}
    options={{
        responsive:true,
        title:{text:'Line Chart',display:true},
        scales:{
            yAxes:[
                {
                    ticks:{
                        autoSkip:true,
                        maxTicksLimit:10,
                        beginAtZero:true
                    },
                    gridLines:{
                        display:false
                    }
                }
            ],
            xAxes:[
                {
                    gridLines:{
                        display:false
                    }
                }
            ]
        }
    }}/>
);
};
export default LineChart

import React from 'react'
import {useSelector,useDispatch} from 'react-redux'
import { buyCake } from '../redux/cakes/CakeActions'
function HooksCakeConatiner() {
    const numofcakes = useSelector(state=>state.numofcakes)
    const dispatch = useDispatch()
    return (
        <div>
            <h2>Number of Cakes - {numofcakes}</h2>
            <button onClick={()=>dispatch(buyCake())}>Buy Cake</button>
        </div>
    )
}

export default HooksCakeConatiner;

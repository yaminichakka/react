import React from 'react'
import {connect} from 'react-redux'
import {buyCake} from '../redux'
function CakeContainer(props) {
    return (
        <div>
            <h2>Number of Cakes - {props.numofcakes}</h2>
            <button onClick={props.buyCake}>Buy Cake</button>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        
        numofcakes : state.numofcakes
    }
}
 
const mapDispatchToProps = dispatch => {
    return {
        buyCake : () => dispatch(buyCake())
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(CakeContainer)

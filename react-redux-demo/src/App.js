import React from 'react';
import {Provider} from 'react-redux'
import store from './redux/store'
import './App.css'
import CakeContainer from './components/CakeContainer';
import HooksCakeConatiner from './components/HooksCakeConatiner';

function App() {
  return (
    <Provider store={store}>
    <div className="App">
      <HooksCakeConatiner/>
     <CakeContainer/>
    </div>
    </Provider>
  );
}

export default App;

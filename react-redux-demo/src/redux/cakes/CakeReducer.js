import {BUY_CAKE} from './CakeTypes'
const initialState = {
    numofcakes:10
}

const cakeReducer = (state = initialState,action) =>
{
    switch(action.type)
    {
        case BUY_CAKE:
            return{
                ...state,
                numofcakes : state.numofcakes - 1
            }
        default: return state
    }
}

export default cakeReducer
import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyD-noJmC_oETAwvD696dn8mn2bsWGhj9G0",
    authDomain: "otp-react-firebase.firebaseapp.com",
    databaseURL: "https://otp-react-firebase.firebaseio.com",
    projectId: "otp-react-firebase",
    storageBucket: "otp-react-firebase.appspot.com",
    messagingSenderId: "327178090997",
    appId: "1:327178090997:web:f76dcf68059e82c74208bc",
    measurementId: "G-RRVSN35LRF"
}
firebase.initializeApp(config);
export default firebase;
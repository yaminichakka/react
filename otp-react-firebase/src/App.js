import React, { Component } from 'react'
import firebase from './firebase'
export class App extends Component {
  setUpRecaptcha = () => {
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
      'size': 'invisible',
      'callback': function(response) {
        // reCAPTCHA solved, allow signInWithPhoneNumber.
        console.log("captcha resolved");
        this.onSignInSubmit();
      }
    });
  }

  onSignInSubmit = (event) => {
  event.preventDefault();
  this.setUpRecaptcha();
  var phoneNumber = "+919705163493";
  var appVerifier = window.recaptchaVerifier;
  firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
      .then(function (confirmationResult) {
        // SMS sent. Prompt user to type the code from the message, then sign the
        // user in with confirmationResult.confirm(code).
        window.confirmationResult = confirmationResult;
        var code = window.prompt("Enter OTP");
        confirmationResult.confirm(code).then(function (result) {
          // User signed in successfully.
          var user = result.user;
          // ...
          console.log("User Signed in");
        }).catch(function (error) {
          // User couldn't sign in (bad verification code?)
          // ...
        });
      }).catch(function (error) {
        // Error; SMS not sent
        // ...
      });
  }
  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="row justify-contentpcenter aligin-items-center">
            <div className="col-sm-6 text-left">
              <h2>Phone Login</h2>
              <form onSubmit={this.onSignInSubmit}>
                <div id="recaptcha-container"></div>
                <button type="submit" className="btn btn-primary">Click to get OTP</button>
              </form>
            </div>

          </div>
        </div>
        
      </div>
    )
  }
}

export default App

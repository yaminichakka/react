import React, { Component } from 'react'
import axios from 'axios'

export class PostFile extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             userid:'',
             title:'',
             body:''
        }
    }
    changeHandler = event =>
    {
        this.setState({ [event.target.name] : event.target.value
           
        })
    }
    submitHandler = event =>
    {
        event.preventDefault();
        console.log(this.state);
        axios.post('https://jsonplaceholder.typicode.com/posts',this.state)
        .then(response => {
            console.log(response)
        })
        .catch(error => {
            console.log(error)
        })
    }
    
    render() {
        
        return (
            <div>
                <form onSubmit={this.submitHandler}>
                    <div>
                        <input type="text" name="userid" value={this.state.userid}  onChange={this.changeHandler}/>
                    </div>
                    <div>
                        <input type="text" name="title" value={this.state.title} onChange={this.changeHandler}/>
                    </div>
                    <div>
                        <input type="text" name="body" value={this.state.body} onChange={this.changeHandler}/>
                    </div>
                    <button>Submit</button>
                </form>
            </div>
        )
    }
}

export default PostFile

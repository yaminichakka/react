import React from 'react';
import './App.css';
//import HookCounterArr from './HookCounterArr';
//import HookCounterPrev from './HookCounterPrev';
//import HookCounterObj from './HookCounterObj';
// import ClickCounter from './ClickCounter';
// import Form from './Form';
//import Ref from './Ref';
// import User from './User';
// import Counter from './Counter';
// import PostFile from './PostFile';
// import HookCounter from './HookCounter';
import ComponentC from './ComponentC';
export const UserContext = React.createContext()
export const ChannelContext = React.createContext()


function App() {
  return (
    <div className="App">
      {/* <Form /> */}
      {/* <Ref/> */}
      {/* <ClickCounter/>
      <User render={(isLoggedIn)=> isLoggedIn ?'Yamini' : 'Chakka'}/> */}
      {/* <Counter render = {(count,clickButton)=>(<ClickCounter count={count} clickButton={clickButton}/>)}/> */}
      {/* <PostFile/> */}
      {/* <HookCounter/> */}
      {/* <HookCounterPrev/> */}
      {/* <HookCounterObj/> */}
      {/* <HookCounterArr/> */}
      <UserContext.Provider value={'yamini'}>
        <ChannelContext.Provider value={'chakka'}>
        <ComponentC/>
        </ChannelContext.Provider>
        </UserContext.Provider>
      
    </div>
  );
}

export default App;

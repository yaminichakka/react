import React, { useState } from 'react'

function HookCounterPrev() {
    const intialstate = 0
    const [count,setCount]=useState(intialstate)
    return (
        <div>
            Count:{count}
            <button onClick={()=>setCount(intialstate)}>Reset</button>
            <button onClick={()=>setCount(count+1)}>Increment</button>
            <button onClick={()=>setCount(count-1)}>Decrement</button>
        </div>
    )
}

export default HookCounterPrev

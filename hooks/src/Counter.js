import React, { Component } from 'react'

export class Counter extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             count : 0
        }
    }
    clickButton = () =>
    {
        this.setState({
           count : this.state.count + 1
        })
    }
    render() {
        const {count} = this.state;
        return (
            <div>
                {this.props.render(this.state.count,this.clickButton)}
            </div>
        )
    }
}

export default Counter

import React, { Component } from 'react'

export class ClickCounter extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             count : 0
        }
    }
    clickButton = () =>
    {
        this.setState({
           count : this.state.count + 1
        })
    }
    declickButton = () =>
    {
        this.setState({
           count : this.state.count - 1
        })
    }
    render() {
        const { count} = this.state;
        return (
            <div>
                <button onClick={this.clickButton}>Clicked {count} Times</button>
                <button onClick={this.declickButton}>Clicked {count} Times</button>
                
            </div>
        )
    }
}

export default ClickCounter

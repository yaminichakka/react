
//redux to store the state
const redux = require('redux')
const reduxlogger = require('redux-logger')
//createStore to store the state of application
const createStore = redux.createStore;
const combinedreducers=redux.combineReducers;
const applyMiddleware = redux.applyMiddleware
const logger = reduxlogger.createLogger()
//console.log('From index.js')
const BUY_CAKE = 'BUY_CAKE'
const BUY_ICECREAM = 'BUY_ICECREAM'

//action and action creator
function buycake()
{
    return {
        type:BUY_CAKE,
        info:'First redux action'
    }  
}
function buyicecream()
{
    return {
        type:BUY_ICECREAM,
        info:'First redux action'
    }  
}

//reducer-(prevState,action)=>newstate

const initialState = 
{
    numofcakes:10,
    numoficecreams:20
}
const initialCakeState = 
{
    numofcakes:10
}
const intialIceState = 
{
    numoficecreams:20
}
//COMBINED REDUCER METHOD
const cakereducer = (state = initialCakeState,action) =>
{
    switch(action.type)
    {
        case BUY_CAKE:
            return{
                ...state,
                numofcakes : state.numofcakes - 1
            }
        default:return state
    }
} 
const icereducer = (state = intialIceState,action) =>
{
    switch(action.type)
    {
        case BUY_ICECREAM:
            return{
                ...state,
                numoficecreams : state.numoficecreams - 1
            }
        default:return state
    }
} 
//SINGLE REDUCER METHOD
//spread operator - copy of state object (...)
// const reducer = (state = initialState,action) =>
// {
//     switch(action.type)
//     {
//         case BUY_CAKE:
//             return{
//                 ...state,
//                 numofcakes : state.numofcakes - 1
//             }
//             case BUY_ICECREAM:
//                 return{
//                     ...state,
//                     numoficecreams : state.numoficecreams - 1
//                 } 
//         default:return state
//     }
// } 
//first step it takes argument as reducer and gets intial state
const rootReducer = redux.combineReducers({
    cake:cakereducer,
    icecream:icereducer
})
const store = createStore(rootReducer,applyMiddleware(logger))
console.log('Intial State',store.getState())
//listener to get the updated state
//const unsubscribe = store.subscribe(()=>console.log('Updated State',store.getState()))
//using logger
const unsubscribe = store.subscribe(()=>{})
//dipstaches the action matches the buycake action and returns the updated actions 
store.dispatch(buycake())
store.dispatch(buyicecream())
unsubscribe()
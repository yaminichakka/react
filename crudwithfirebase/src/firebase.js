import * as firebase from 'firebase';
var firebaseConfig = {
    apiKey: "AIzaSyCRUWbuBtOu8JPoO18auVL0FRDDkdQhVZI",
    authDomain: "reactcrudfirebase-d8fed.firebaseapp.com",
    databaseURL: "https://reactcrudfirebase-d8fed.firebaseio.com",
    projectId: "reactcrudfirebase-d8fed",
    storageBucket: "reactcrudfirebase-d8fed.appspot.com",
    messagingSenderId: "275746787224",
    appId: "1:275746787224:web:d4893e6db4d283a93f25ed",
    measurementId: "G-7PXL6S4MK9"
  };
  // Initialize Firebase
  var fireDb = firebase.initializeApp(firebaseConfig);

  export default fireDb.database().ref();
import React,{useState} from 'react';
import './App.css';
// import {FaReact} from 'react-icons/fa'
// import {MdAndroid} from 'react-icons/md'
import Modal from 'react-modal'

Modal.setAppElement('#root')
function App() {
  const [modalIsOpen,setModalIsOpen] = useState(false)
  return (
    <div className="App">
      {/* <FaReact color='purple' size='10rem'/>
      <MdAndroid color='blue' size='10em'/> */}
      <button onClick={()=>setModalIsOpen(true)}>Open Modal</button>
      <Modal isOpen={modalIsOpen} onRequestClose={()=>setModalIsOpen(false)} style={{
        overlay:{
          backgroundColor:'grey'
        },
      content:{
        color:'blue'
      }}
      }>
        <h2>Modal Title</h2>
        <p>Modal body</p>
        <div>
          <button onClick={()=>setModalIsOpen(false)}>Close</button>
        </div>
      </Modal>
    </div>
  );
}

export default App;

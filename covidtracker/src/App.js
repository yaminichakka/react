import React, { Component } from 'react'
import './App.css';
import Cards from './components/Cards/Cards';
import Chart from './components/Chart/Chart';
import CountryPicker from './components/CountryPicker/CountryPicker'
import {fetchData} from './api';
import corona from './image.png';
class App extends Component {
  state = {
    data : {},
    country:'',
  }
  async componentDidMount()
  {
    const fetchedData = await fetchData();
    this.setState({data:fetchedData})
  }
  handleCountryChange = async(country) =>
  {
    const fetchedData = await fetchData(country);
    this.setState({data:fetchedData,country:country});
    //console.log(country);
  }
  render() {
    const {data,country}=this.state;
    return (
      <div className="container">
        <img src={corona} className="image" alt="COVID-19"/><br/>
        <Cards data={data}/><br></br><br></br>
        <CountryPicker handleCountryChange={this.handleCountryChange}/><br></br><br></br>
        <Chart data={data} country={country}/>
        
      </div>
    )
  }
}

export default App;
